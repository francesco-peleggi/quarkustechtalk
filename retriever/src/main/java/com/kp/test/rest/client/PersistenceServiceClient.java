package com.kp.test.rest.client;


import com.kp.test.model.dto.Document;
import com.kp.test.model.dto.KeywordList;
import com.kp.test.model.dto.Match;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@RegisterRestClient(configKey = "persistence-service")
@Produces(MediaType.APPLICATION_JSON)
public interface PersistenceServiceClient {


    @POST
    @Path("/match")
    Uni<Match> getMatch(KeywordList keywords);

    @GET
    @Path("doc/stream")
    Multi<Document> streamDocuments();

}
