package com.kp.test.rest.controller;


import com.kp.test.model.dto.KeywordList;
import com.kp.test.model.dto.Match;
import com.kp.test.service.MatchService;
import io.smallrye.mutiny.Uni;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Path("/match")
public class RetrieverRestController {

    @Inject
    MatchService matchService;

    Logger log = LoggerFactory.getLogger(RetrieverRestController.class);

    @POST
    @Path("/keywords")
    public Uni<Match> requestMatch(KeywordList keywords){
        log.info("Match requested");
        return matchService.requestMatch(keywords);
    }


}
