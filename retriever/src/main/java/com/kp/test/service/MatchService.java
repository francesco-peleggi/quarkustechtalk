package com.kp.test.service;

import com.kp.test.model.dto.KeywordList;
import com.kp.test.model.dto.Match;
import com.kp.test.rest.client.PersistenceServiceClient;
import io.smallrye.mutiny.Uni;
import io.vertx.core.AbstractVerticle;
import io.vertx.ext.web.client.HttpResponse;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.codec.BodyCodec;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.JsonObject;
import java.time.Duration;

@ApplicationScoped
public class MatchService{          //extends AbstractVerticle



    @Inject
    @RestClient
    private PersistenceServiceClient docServiceClient;
    @Inject
    @Channel("matches")
    Emitter<Match> emitter;

    public Uni<Match> requestMatch(KeywordList keywords){
        Uni<Match> match = docServiceClient.getMatch(keywords)
                .onItem().call(this::sendToKafka)
                .ifNoItem().after(Duration.ofMillis(1000))
                    .fail()
                .onFailure()
                    .recoverWithItem(new Match("NOPE"));

        return match;

    }


    private Uni<Match> sendToKafka(Match match){
        emitter.send(match);
        return Uni.createFrom().item(match);
    }
}



//    public Uni<Match> requestMarchVertx(KeywordList keywords){
//        WebClient.create(vertx)
//    }
//
//
//    public String matchStreamed(){
//        WebClient client = WebClient.create(vertx);
//        client
//                .get(8080, "localhost:8080", "/streamer/doc/stream")
//                .as(BodyCodec.json(Document.class))
//                .send(ar ->{
//                    if(ar.succeeded()){
//                        HttpResponse<Document> response = ar.result();
//                        System.out.println("got data : "+response.bodyAsString());
//                        ;                    }
//                });
//        return ""; }


