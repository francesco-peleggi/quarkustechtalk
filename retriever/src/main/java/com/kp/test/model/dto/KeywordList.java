package com.kp.test.model.dto;
import io.smallrye.mutiny.Uni;

import java.util.List;
import java.util.Random;

public class KeywordList {

    public String topic;
    public List<String> kWords;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public List<String> getkWords() {
        return kWords;
    }

    public void setkWords(List<String> kWords) {
        this.kWords = kWords;
    }

}
