package com.kp.test.kafka;

import com.kp.test.model.Match;
import com.kp.test.model.Stats;
import io.smallrye.mutiny.Multi;
import io.smallrye.reactive.messaging.annotations.Broadcast;
import org.eclipse.microprofile.reactive.messaging.Acknowledgment;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;

import javax.enterprise.context.ApplicationScoped;
import java.util.HashMap;
import java.util.Map;

@ApplicationScoped
public class MatchProcessor {

    @Incoming("matches")
    @Outgoing("stats")
    @Broadcast
    @Acknowledgment(Acknowledgment.Strategy.NONE)
    public Multi<Stats> process(Multi<Match> matches){

        return matches.onItem().scan(Stats::new,this::compute);
    }

    private Stats compute(Stats s,Match m){

        String topic = m.getTopic();
        Map<String,Integer> mostSearchedCounter = new HashMap<>();
        Stats.MatchElement max = new Stats.MatchElement();
        if(s.getTopicSearches()!= null && s.getTopicSearches().containsKey(m.getTopic())){
            s.getTopicSearches().put(topic,s.getTopicSearches().get(topic)+1);
        }else {
            if(s.getTopicSearches()==null)
                s.setTopicSearches(new HashMap<>());
            s.getTopicSearches().put(topic, 1);
        }
        if(m.getMatches()!=null){
        m.getMatches().keySet().forEach(kWord->{

            Integer matchCounter = m.getMatches().get(kWord);
            if(!mostSearchedCounter.containsKey(kWord))
                mostSearchedCounter.put(kWord,matchCounter);

            if(mostSearchedCounter.get(kWord)<matchCounter){
                max.setKeyword(kWord);
                max.setCount(matchCounter);
                mostSearchedCounter.put(kWord,matchCounter);
            }
        });}
        s.setMaxResultsPerTopic(topic,max);
        return s;
    }
}
