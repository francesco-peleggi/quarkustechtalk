package com.kp.test.util;

import com.kp.test.model.Match;
import io.quarkus.kafka.client.serialization.JsonbDeserializer;

public class MatchDeserializer extends JsonbDeserializer<Match> {
    public MatchDeserializer(){super(Match.class);};
}
