package com.kp.test.util;

import com.kp.test.model.Match;
import com.kp.test.model.Stats;
import io.quarkus.kafka.client.serialization.JsonbDeserializer;

public class StatsDeserializer extends JsonbDeserializer<Stats> {
    public StatsDeserializer(){super(Stats.class);}
}
