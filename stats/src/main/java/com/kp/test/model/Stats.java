package com.kp.test.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Stats implements Serializable {
    @Override
    public String toString() {
        return "Stats{" +
                "topicSearches=" + topicSearches +
                ", maxResultsPerTopic=" + maxResultsPerTopic +
                '}';
    }

    private Map<String,Integer> topicSearches;
    private Map<String,MatchElement> maxResultsPerTopic;

    public Map<String, MatchElement> getMaxResultsPerTopic() {
        return maxResultsPerTopic;
    }

    public void setMaxResultsPerTopic(Map<String, MatchElement> maxResultsPerTopic) {
        this.maxResultsPerTopic = maxResultsPerTopic;
    }
    public void setMaxResultsPerTopic(String kWord, MatchElement maxResultsPerTopic) {
        this.maxResultsPerTopic = (Map<String, MatchElement>) new HashMap<>().put(kWord,maxResultsPerTopic);
    }

    public Map<String, Integer> getTopicSearches() {
        return topicSearches;
    }

    public void setTopicSearches(Map<String, Integer> topicSearches) {
        this.topicSearches = topicSearches;
    }

    public static class MatchElement implements Serializable{
        private String keyword;
        private Integer count;

        public MatchElement(){}
        public MatchElement(String keyword,Integer count){this.keyword=keyword;this.count=count;}
        public void setCount(Integer count){this.count=count;}
        public void setKeyword(String keyword){this.keyword=keyword;}

        public String getKeyword(){return keyword;}
        public Integer getCount(){return count;}
    }

}
