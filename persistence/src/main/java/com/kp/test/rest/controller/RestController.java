package com.kp.test.rest.controller;

import com.kp.test.model.dto.Match;
import com.kp.test.model.entity.Document;
import com.kp.test.model.entity.KeywordList;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import org.jboss.resteasy.annotations.SseElementType;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;

@Path("/streamer")
@Produces(MediaType.APPLICATION_JSON)
public class RestController {




    @POST
    @Path("match")
    public Uni<Match> matcher(KeywordList req){

        Match m = new Match();
        Map<String,Integer> matchesMap = new HashMap();

        m.setTopic(req.getTopic());

        Document.streamAll()
                .filter(c -> ((Document)c).getContent().toLowerCase().contains(req.getTopic().toLowerCase()))

                .subscribe()

                .asStream()
                .forEach(x->{
                    String content = ((Document)x).getContent();
                    req.getkWords().forEach(
                            kWord ->{
                                if(content!=null && content.contains(kWord)){
                                    if(matchesMap.get(kWord)==null){
                                        matchesMap.put(kWord,1);
                                    }else {
                                        matchesMap.put(kWord, matchesMap.get(kWord) + 1);
                                    }
                                }
                            }
                    );
                });
        m.setTopic(req.getTopic().toUpperCase());
        m.setMatches(matchesMap);
        return Uni.createFrom().item(m);
    }

    @GET
    @Path("/doc/stream")
    @Produces(MediaType.SERVER_SENT_EVENTS)
    @SseElementType(MediaType.APPLICATION_JSON)
    public Multi<Document> streamAllDocs(){
        return Document.streamAll();
    }
}
