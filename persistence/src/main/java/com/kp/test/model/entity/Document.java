package com.kp.test.model.entity;

import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.reactive.ReactivePanacheMongoEntity;
import io.smallrye.mutiny.Uni;

import java.util.Random;

@MongoEntity(collection = "docs")
public class Document extends ReactivePanacheMongoEntity {
    public String content;
    public String title;
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public static Uni<Document> findRandom(){
        Random rng = new Random();

        return Document.count().map(l->rng.nextInt(l.intValue()))
                .flatMap(index -> {
                    return Document.findAll().page(index,1).firstResult();
                });
    }

}
