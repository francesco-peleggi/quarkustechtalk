package com.kp.test.model.entity;
import io.quarkus.mongodb.panache.MongoEntity;
import io.quarkus.mongodb.panache.reactive.ReactivePanacheMongoEntity;
import io.smallrye.mutiny.Uni;

import java.util.List;
import java.util.Random;

@MongoEntity(collection="kWordList")
public class KeywordList extends ReactivePanacheMongoEntity {

    public String topic;
    public List<String> kWords;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public List<String> getkWords() {
        return kWords;
    }

    public void setkWords(List<String> kWords) {
        this.kWords = kWords;
    }

    public static Uni<KeywordList> findRandom(){
        Random rng = new Random();

        return KeywordList.count().map(l->rng.nextInt(l.intValue()))
                .flatMap(index -> {
                    return KeywordList.findAll().page(index,1).firstResult();
                });
    }
}
