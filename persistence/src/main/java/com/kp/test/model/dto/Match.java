package com.kp.test.model.dto;

import java.util.List;
import java.util.Map;

public class Match {

    public String topic;
    public Map<String,Integer> matches;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Map<String, Integer> getMatches() {
        return matches;
    }

    public void setMatches(Map<String, Integer> matches) {
        this.matches = matches;
    }


}
